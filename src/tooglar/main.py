import argparse
import os
import re
from datetime import date, datetime, timedelta, time
from pathlib import Path
from typing import Optional, List

import pytz
import requests
import sys
import yaml
from tabulate import tabulate

BASE_URL = "https://api.track.toggl.com/api/v8/"
TIME_ENTRY_URL = BASE_URL + "time_entries"
PROJECT_URL = BASE_URL + "projects"

Auth = (str, str)


def read_config() -> dict:
    home_dir = Path.home()
    config_file = os.path.join(home_dir, ".tooglar.yaml")
    with open(config_file) as f:
        return yaml.safe_load(f)


def read_project_name(project_id: int) -> str:
    res = requests.get(f"{PROJECT_URL}/{project_id}", auth=auth)
    res.raise_for_status()
    if res.status_code != 200:
        print(res.text)
        res.raise_for_status()
    return res.json()["data"]["name"]


def read_entries(from_datetime: Optional[datetime], to_datetime: Optional[datetime]) -> List[dict]:
    params = dict()
    if from_datetime:
        params["start_date"] = from_datetime.isoformat()
    if to_datetime:
        params["end_date"] = to_datetime.isoformat()

    res = requests.get(TIME_ENTRY_URL, params=params, auth=auth)
    if res.status_code != 200:
        print(res.text)
        res.raise_for_status()
    return res.json()


def read_entries_on_day(day: date) -> List[dict]:
    day_start: datetime = timezone.localize(datetime.combine(day, datetime.min.time()))
    day_end: datetime = day_start + timedelta(days=1)
    return read_entries(from_datetime=day_start, to_datetime=day_end)


def create_entry(entry: dict, auth: Auth) -> None:
    body = {"time_entry": entry}
    res = requests.post(TIME_ENTRY_URL, json=body, auth=auth)
    if res.status_code != 200:
        print(res.text)
        res.raise_for_status()


def clone_entries(from_date: date, to_date: date) -> None:
    entries_on_day = len(read_entries_on_day(to_date))
    if entries_on_day > 0 and not args.force:
        raise ValueError(f"There are already time entries on {to_date} (--force not supplied)")

    from_datetime_start = timezone.localize(datetime.combine(from_date, datetime.min.time()))
    from_datetime_end = from_datetime_start + timedelta(days=1)
    offset: timedelta = to_date - from_date

    old_entries = read_entries(from_datetime_start, from_datetime_end)

    for entry in old_entries:
        new_entry = dict()
        new_entry["start"] = (datetime.fromisoformat(entry["start"]) + offset).isoformat()
        new_entry["created_with"] = config["application_name"] or "tooglar"
        for key in ["description", "duration", "billable", "wid", "pid", "uid"]:
            new_entry[key] = entry[key]

        create_entry(new_entry, auth)

    print(f"Cloned {len(old_entries)} entries to {to_date}")


def parse_time(time_string: str) -> time:
    stripped = time_string.strip()
    match = re.match(r"^(\d{1,2})(?::(\d{2})(?::(\d{2}))?)?$", stripped)
    if match:
        hour = int(match[1])
        minute = int(match[2] or 0)
        second = int(match[3] or 0)
        return time(hour=hour, minute=minute, second=second)
    else:
        ValueError(f"Invalid time given: {time_string}")


def schedule_entries(day: date, schedule_name: Optional[str], schedules: dict) -> None:
    schedule_name: str = schedule_name or day.strftime("%A").lower()
    if schedule_name not in schedules:
        raise ValueError(f"Unknown schedule name '{schedule_name}'")

    entries_on_day = len(read_entries_on_day(day))
    if entries_on_day > 0 and not args.force:
        raise ValueError(f"There are already time entries on {day} (--force not supplied)")

    schedule = schedules[schedule_name]
    if isinstance(schedule, list):
        for schedule_entry in schedule:
            new_entry = dict()
            if "start" not in schedule_entry:
                raise ValueError("schedule entry must contain start time")
            if "end" not in schedule_entry:
                raise ValueError("schedule entry must contain end time")

            start_time = parse_time(schedule_entry["start"])
            end_time = parse_time(schedule_entry["end"])
            duration = (datetime.combine(date.min, end_time) - datetime.combine(date.min, start_time)).seconds
            new_entry["start"] = timezone.localize(datetime.combine(day, start_time)).isoformat()
            new_entry["stop"] = timezone.localize(datetime.combine(day, end_time)).isoformat()
            new_entry["duration"] = duration
            if "project" in schedule_entry:
                new_entry["pid"] = int(schedule_entry["project"])
            new_entry["created_with"] = config["application_name"] or "tooglar"
            for key in ["description", "billable"]:
                if key in schedule_entry:
                    new_entry[key] = schedule_entry[key]
            create_entry(new_entry, auth)
        print(f"Created {len(schedule)} entries on {day}")


def get_entries(day: date) -> None:
    entries = read_entries_on_day(day)
    rows = list(reversed([[datetime.fromisoformat(e["start"]).replace(tzinfo=pytz.utc).astimezone(tz=timezone).strftime("%H:%M:%S"),
                           timedelta(seconds=e["duration"]),
                           e["description"] if "description" in e else "",
                           e["pid"] if "pid" in e else None,
                           "$" if "billable" in e and e["billable"] is True else "x"] for e in entries]))
    project_names = {project_id: read_project_name(project_id) for project_id in {row[3] for row in rows} if project_id}
    for row in rows:
        project_id = row[3]
        if project_id:
            project_name = project_names[project_id]
            row[3] = f"{project_name} (id {project_id})" if args.print_ids else project_name
    table = tabulate(rows, headers=("START", "DURATION", "DESCRIPTION", "PROJECT", "BILLABLE"))
    print(table)


if __name__ == "__main__":
    config = read_config()
    schedules = config["schedules"] if "schedules" in config and isinstance(config["schedules"], dict) else dict()

    parser = argparse.ArgumentParser(description="tooglin' things")
    subparsers = parser.add_subparsers(dest="command")

    clone_parser = subparsers.add_parser("clone")
    clone_parser.add_argument("--from", dest="from_day", required=True, type=date.fromisoformat)
    clone_parser.add_argument("--to", dest="to_day", required=True, type=date.fromisoformat)
    clone_parser.add_argument("--force", dest="force", action="store_true")

    schedule_parser = subparsers.add_parser("schedule")
    schedule_parser.add_argument("--day", dest="day", required=True, type=date.fromisoformat)
    schedule_parser.add_argument("--schedule", dest="schedule", choices=schedules.keys())
    schedule_parser.add_argument("--force", dest="force", action="store_true")

    get_parser = subparsers.add_parser("get")
    get_parser.add_argument("--day", dest="day", required=True, type=date.fromisoformat)
    get_parser.add_argument("--ids", dest="print_ids", action="store_true")

    args: argparse.Namespace = parser.parse_args()
    timezone = pytz.timezone(config["timezone"])
    auth = (config["api_key"], "api_token")

    try:
        if args.command == "clone":
            clone_entries(args.from_day, args.to_day)
        elif args.command == "schedule":
            schedule_entries(args.day, args.schedule, schedules)
        elif args.command == "get":
            get_entries(args.day)
        else:
            parser.print_help()
            sys.exit(1)
    except ValueError as e:
        print(f"ERROR: {e}")
